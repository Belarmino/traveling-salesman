'''
USAGES:
	- python3 [number of executions] [file to read - a tsp file]
'''


from City import City
from TSP import TSP

import sys
import timeit

def main(argv):
	print ("Iniciando Caxeiro Viajante..")
	number_exec = int(argv[1])

	cities = []
	fileContent = open(argv[3], 'r').read()
	
	# processaento dos arquivos formatados
	lines = fileContent.split("\n")
	filename = lines[0]
	custo_otimo = lines[1]
	custo_otimo = int(custo_otimo[8:]) 
	print(len(lines))
	for line in lines[7:]:
		try:
			if(line == "EOF"):
				break
			aux = line.split(" ")
			# aloca id, posicao X e posicao Y
			city = City(aux[0], aux[1], aux[2])
			cities.append(city)
		except Exception as e:
			print ("1 " + str(e) + " city :" + str(city.id))
			break

	tsp = TSP()
	M = tsp.get_distancia_matrix(cities)
	
	# Printando matriz
	# tsp.print_matrix(M)
	# exit()

	start_time = timeit.default_timer()

	custo_Grasp = float("inf")
	tour, custo_Construcao = tsp._TSP(cities,M)
	print("Custo Construtivo:"+str(custo_Construcao))
	print("Percurso Construtivo: ")
	print(str(tsp.print_rota(tour)))
	print("GAP Construtivo: %.2f" %tsp._GAP(custo_Construcao,custo_otimo))

	for i in range(number_exec):
		tour, custo = tsp._Grasp(cities, M,float(argv[2]))
		if custo < custo_Grasp:
			print("Tentativa "+str(i))
			print("Novo Custo:"+str(custo))
			tour_final = tour
			custo_Grasp = custo

	print("Percurso Meta_Heuristico: ")
	print(str(tsp.print_rota(tour_final)))
	print("Custo Meta_Heuristico: "+str(custo_Grasp))
	print("GAP Meta_Heuristico: %.2f"%tsp._GAP(custo_Grasp,custo_otimo))
	print ("---------------------------------------------\n\n")


if __name__ == "__main__":
	main(sys.argv)

