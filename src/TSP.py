import math
import collections
import random

class TSP:
	""" Define os algoritmos para resolucao do TSP  """
	
	def __init__(self):
		pass
	
	def get_distancia_matriz(self, cidades):
		'''transforma a lista de cidades em uma matriz de custos'''

		size = len(cidades) + 1

		M = [ [ 0 for _ in range(size) ] for __ in range(size) ]

		for i in cidades:
			for j in cidades:
				M[i.id][j.id] = self.distancia(i, j)

		return M

	def print_matriz(self, M):
		'''imprime a matriz de custo de uma forma mais legivel'''
		print('\n'.join([''.join(['{:5}'.format(item) for item in row]) for row in M]))

	def distancia(self, c1, c2):
		'''Calcula distancia ecludiana entre dois objectos cidade'''

		xd = c1.x - c2.x 
		yd = c1.y - c2.y 
		dij = int( math.sqrt( xd*xd + yd*yd) + 0.5 )

		# return infinito se o calculo for efetuado para a mesma cidade
		if dij == 0:
			return float("inf")
		else:
			return dij
	
	def vizinho_proximo(self, A, cities, M):
		'''
		retorna o vizinho mais proximo
		A => cidade atual
		Cities => possiveis vizinhos
		M => matriz de distancias
		'''
		
		min_d = float("inf")
		min_city = None
		for i in cities:
			ci = cities[i]

			if M[A.id][ci.id] < min_d:
				min_d = M[A.id][ci.id]
				min_city = ci

		return min_city

	def distancia_cidades(self, c1, c2, M):
		'''retorna distancia entre 2 cidades'''
		return M[c1.id][c2.id]

	def print_rota(self, cidades):
		string = ""
		for i in cidades:
			string+= "->"
			string += str(i.id) + " "

		return string

	def get_custo(self, rota, M):
		'''
		Return custo of a rota
		'''
		
		deque_cidades = collections.deque(rota)

		total_custo = 0

		current_city = deque_cidades.popleft()

		while len(deque_cidades):
			next_city = deque_cidades.popleft()

			total_custo += self.distancia_cidades(current_city, next_city, M)

			current_city = next_city

		return total_custo

	def _swap(self, rota, i1, i2):
		clone = rota[:]

		aux = clone[i1]
		clone[i1] = clone[i2]
		clone[i2] = aux

		return clone

	def _2opt(self, rota, M):
		'''
		movimento de vizinhanca 2opt

		Utiliza o movimento ate encontrar um trajeto minimo local 
		'''
		
		min_custo = self.get_custo(rota, M)
		min_rota = rota[:]

		for i in range(2, len(min_rota) - 2 ):
			for j in range(i + 1, len(min_rota)):

				new_rota = self._swap(min_rota, i, j)
				new_custo = self.get_custo(new_rota, M)

				if new_custo < min_custo:
					min_custo = new_custo
					min_rota = new_rota
		# print("_2opt")
		return min_custo,min_rota

	def _insertion(self, rota, M):
		min_custo = self.get_custo(rota, M)
		min_rota = rota[:]

		for i in range(2,len(min_rota)):
			for j in range(len(min_rota)-1):
				if(i != j):
					new_rota = self._swap(min_rota, i, j)
					new_custo = self.get_custo(new_rota, M)

					if new_custo < min_custo:
						min_custo = new_custo
						min_rota = new_rota
						# print("_insertion")
						return min_custo,min_rota
		return min_custo,min_rota


	def VND(self, rota, M):
		'''
		O vnd tenta melhorar a rota utilizando os movimentos de vizinhanca 2opt e reinsertion.
		
		O algoritmo ira utilizar o 2opt ate que se atinja um minimo local (sem mais melhoras),
		apos o minimo local do 2opt, o reinsertion eh chamado, Se houver ganhas na rota o algoritmo repete todo 
		o processo com o 2opt. 
		
		ele so vai parar quando nem o 2opt nem o reinsertion obtiverem resultados melhores

		return
			melhor rota
		'''
		
		min_custo = self.get_custo(rota, M)
		min_rota = rota[:]

		size = len(rota)

		custo_2opt = float("inf")

		while True:
			# print("here")
			custo_insertion,rota_insertion = self._insertion(min_rota,M)
			if custo_insertion < min_custo:
				min_rota = rota_insertion
				min_custo = custo_insertion
				# print("Melhorou Insertion")
				# print("travou aqui vnd")
				continue
			# # chegou no minimo local (insertion e 2opt nao conseguiram nenhum melhoramento)
			elif custo_insertion == custo_2opt:
				# print("saiu insert")
				break
				# Roda o _2opt 
			else:
				rota_atual = rota_insertion
				while True:
			 		#print ("\nStarting reinsertion...\n")
			 		custo_2opt,rota_2opt = self._2opt(rota_atual, M)
					# se o reinsertion obter resutados, volta pro 2opt
			 		if custo_2opt < min_custo:
						 min_rota = rota_2opt
						 min_custo = custo_2opt
						#  print("Melhorou 2opt")
						 continue

			 		elif custo_2opt == min_custo: # se nao conseguir melhores resultados => volta pro 2opt
						#  print("saiu 2opt")
						 break
			
		return min_rota
	#Greedy Randomized Adaptive Search    
	def _RCL(self, current_el, mapa, M, alpha):
		"""
		Controi a lista de bons candidatos.
		Se alpha = 0 => vai ser uma lista totalmente gulosa (sem variacao)
		se alpha = 1 => vaiser uma lista totalmente aleatoria
		"""
		#print("mapa:"+mapa)
		costs_map = { chave: self.distancia_cidades(current_el, value, M) for (chave, value) in mapa.items() }
		#print("COSTS_MAPA:"+str(costs_map))
		chave_max = max(costs_map.keys(), key=(lambda chave: costs_map[chave]))
		chave_min = min(costs_map.keys(), key=(lambda chave: costs_map[chave]))

		min_cost = costs_map[chave_min] # pior custo
		max_cost = costs_map[chave_max] # melhor custokr

		alpha_offset = min_cost + (alpha * (max_cost - min_cost))

		RCL = [ mapa[chave] for (chave, valor) in costs_map.items() if valor <= alpha_offset ]

		return RCL
	
	def _GAP(self,custo,otimo):
		return ((custo-otimo)/otimo) * 100

	def _TSP(self, cidades, M,):
		mapa = {i.id: i for i in cidades}

		# array melhor rota
		solucao_construcao = []

		first_el = mapa[1]
		del mapa[1]

		solucao_construcao.append(first_el)
		custo = 0.0
		current_el = first_el
		# perocrrer mapa
		while len(mapa):
			current_el = self.vizinho_proximo(current_el,mapa,M)

			del mapa[current_el.id]
			solucao_construcao.append(current_el)
		solucao_construcao.append(first_el)
		custo = self.get_custo(solucao_construcao, M)
		return solucao_construcao, custo

	def _Grasp(self, cidades, M,alfa):
		'''
		Run VND

		Parameters:
			cidades: dictionay of cidades
			M: distancia matriz

		Return:
			best rota in an array
			respective custo
		'''
		mapa = {i.id: i for i in cidades}

		# array melhor rota
		solucao = []
		first_el = mapa[1]
		del mapa[1]

		solucao.append(first_el)
		custo = 0.0
		current_el = first_el
		while len(mapa):

			rangeCandidates = self._RCL(current_el, mapa, M, alfa)
			# adiciona um dos bons candidatos à solução
			current_el = random.choice(rangeCandidates) 

			del mapa[current_el.id]
			solucao.append(current_el)
		# print(str(self.print_rota(solucao)))
		custo = self.get_custo(solucao, M)

		VND_solucao = self.VND(solucao, M)
		VND_solucao.append(VND_solucao[0])
		VND_custo = self.get_custo(VND_solucao, M)

		# se solução é melhor substitui para o retorno
		if VND_custo < custo:
			solucao = VND_solucao
			custo = VND_custo

		return solucao, custo